	package clasesNotas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NotaDAO {
    private Connection conexion;

    public NotaDAO() {
        try {
            conexion = ConexionBD.conectar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertarNota(Nota nota) {
        String sql = "INSERT INTO Notas ( id_estudiante, asignatura, grado, nota_definitiva ) VALUES (?, ?, ?, ?)";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setInt(1, nota.getIdEstudiante());
            ps.setString(2, nota.getAsignatura());
            ps.setString(3, nota.getGrado());
            ps.setDouble(4, nota.getNotaDefinitiva());
    
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public void actualizarNota(Nota nota) {
        String sql = "UPDATE  Notas SET grado=?, nota_definitiva=? WHERE id_estudiante=? AND asignatura=?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setString(1, nota.getGrado());
            ps.setDouble(2, nota.getNotaDefinitiva());
            ps.setInt(3, nota.getIdEstudiante());
            ps.setString(4, nota.getAsignatura());
    
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public Nota consultarNota(int idEstudiante, String asignatura) {
        Nota nota = null;
        String sql = "SELECT * FROM Notas WHERE id_estudiante = ? AND asignatura = ?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setInt(1, idEstudiante);
            ps.setString(2, asignatura);
            ResultSet rs = ps.executeQuery();
    
            if (rs.next()) {
                nota = new Nota(
                    rs.getInt("id_estudiante"),
                    rs.getString("asignatura"),
                    rs.getString("grado"),
                    rs.getDouble("nota_definitiva")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nota;
    }
    

    public void eliminarNota(int idEstudiante, String asignatura) {
        String sql = "DELETE FROM Notas WHERE id_estudiante = ? AND asignatura = ?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setInt(1, idEstudiante);
            ps.setString(2, asignatura);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
