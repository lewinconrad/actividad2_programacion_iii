package clasesNotas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EstudianteDAO {
    private Connection conexion;

    public EstudianteDAO() {
        try {
            conexion = ConexionBD.conectar();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertarEstudiante(Estudiante estudiante) {
        String sql = "INSERT INTO Estudiantes ( nombres, apellidos, numero_identificacion, grado, edad, genero, telefono ) VALUES    (?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setString(1, estudiante.getNombres());
            ps.setString(2, estudiante.getApellidos());
            ps.setString(3, estudiante.getNumeroIdentificacion());
            ps.setString(4, estudiante.getGrado());
            ps.setInt(5, estudiante.getEdad());
            ps.setString(6, estudiante.getGenero());
            ps.setString(7, estudiante.getTelefono());
    
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void actualizarEstudiante(Estudiante estudiante) {
        String sql = "  UPDATE  Estudiantes SET nombres=?, apellidos=?, grado=?, edad=?, genero=?, telefono=? WHERE   numero_identificacion=?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setString(1, estudiante.getNombres());
            ps.setString(2, estudiante.getApellidos());
            ps.setString(3, estudiante.getGrado());
            ps.setInt(4, estudiante.getEdad());
            ps.setString(5, estudiante.getGenero());
            ps.setString(6, estudiante.getTelefono());
            ps.setString(7, estudiante.getNumeroIdentificacion());
    
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public Estudiante consultarEstudiante(String numeroIdentificacion) {
        Estudiante estudiante = null;
        String sql = "SELECT * FROM Estudiantes WHERE numero_identificacion = ?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setString(1, numeroIdentificacion);
            ResultSet rs = ps.executeQuery();
    
            if (rs.next()) {
                estudiante = new Estudiante(
                    rs.getString("nombres"),
                    rs.getString("apellidos"),
                    rs.getString("numero_identificacion"),
                    rs.getString("grado"),
                    rs.getInt("edad"),
                    rs.getString("genero"),
                    rs.getString("telefono")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return estudiante;
    }
    

    public void eliminarEstudiante(String numeroIdentificacion) {
        String sql = "DELETE FROM Estudiantes WHERE numero_identificacion = ?";
    
        try (PreparedStatement ps = conexion.prepareStatement(sql)) {
            ps.setString(1, numeroIdentificacion);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
