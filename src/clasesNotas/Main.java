package clasesNotas;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        EstudianteDAO estudianteDAO = new EstudianteDAO();
        NotaDAO notaDAO = new NotaDAO();

        boolean continuar = true;

        while (continuar) {
            System.out.println("Seleccione una opción:");
            System.out.println("1. Agregar estudiante");
            System.out.println("2. Actualizar estudiante");
            System.out.println("3. Eliminar estudiante");
            System.out.println("4. Agregar nota");
            System.out.println("5. Actualizar nota");
            System.out.println("6. Eliminar nota");
            System.out.println("7. Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
            	// Lógica para agregar estudiante
                case 1:
                	Scanner estudianteScanner = new Scanner(System.in);
                    
                    System.out.println("Ingrese los datos del estudiante:");
                    System.out.print("Nombres: ");
                    String nombres = estudianteScanner.nextLine();
                    System.out.print("Apellidos: ");
                    String apellidos = estudianteScanner.nextLine();
                    System.out.print("Número de identificación: ");
                    String numeroIdentificacion = estudianteScanner.nextLine();
                    System.out.print("Grado: ");
                    String grado = estudianteScanner.nextLine();
                    System.out.print("Edad: ");
                    int edad = estudianteScanner.nextInt();
                    estudianteScanner.nextLine(); // Limpiar el buffer
                    System.out.print("Género: ");
                    String genero = estudianteScanner.nextLine();
                    System.out.print("Teléfono: ");
                    String telefono = estudianteScanner.nextLine();
                    
                    Estudiante nuevoEstudiante = new Estudiante(nombres, apellidos, numeroIdentificacion, grado, edad, genero, telefono);
                    estudianteDAO.insertarEstudiante(nuevoEstudiante);
                    System.out.println("Estudiante agregado correctamente");
                    break;
                // Lógica para actualizar estudiante
                case 2:
                	Scanner updateEstudianteScanner = new Scanner(System.in);

                    System.out.print("Ingrese el número de identificación del estudiante a actualizar: ");
                    String idEstudianteActualizar = updateEstudianteScanner.nextLine();

                    Estudiante estudianteActualizar = estudianteDAO.consultarEstudiante(idEstudianteActualizar);

                    if (estudianteActualizar != null) {
                        System.out.println("Ingrese los nuevos datos del estudiante:");
                        
                        System.out.print("Nuevos nombres: ");
                        String nuevosNombres = updateEstudianteScanner.nextLine();
                        System.out.print("Nuevos apellidos: ");
                        String nuevosApellidos = updateEstudianteScanner.nextLine();
                        System.out.print("Nuevo número de identificación: ");
                        String nuevoNumeroIdentificacion = updateEstudianteScanner.nextLine();
                        System.out.print("Nuevo grado: ");
                        String nuevoGrado = updateEstudianteScanner.nextLine();
                        System.out.print("Nueva edad: ");
                        int nuevaEdad = updateEstudianteScanner.nextInt();
                        updateEstudianteScanner.nextLine(); // Limpiar el buffer
                        System.out.print("Nuevo género: ");
                        String nuevoGenero = updateEstudianteScanner.nextLine();
                        System.out.print("Nuevo teléfono: ");
                        String nuevoTelefono = updateEstudianteScanner.nextLine();

                        // Actualizar los datos del estudiante consultado
                        estudianteActualizar.setNombres(nuevosNombres);
                        estudianteActualizar.setApellidos(nuevosApellidos);
                        estudianteActualizar.setNumeroIdentificacion(nuevoNumeroIdentificacion);
                        estudianteActualizar.setGrado(nuevoGrado);
                        estudianteActualizar.setEdad(nuevaEdad);
                        estudianteActualizar.setGenero(nuevoGenero);
                        estudianteActualizar.setTelefono(nuevoTelefono);

                        estudianteDAO.actualizarEstudiante(estudianteActualizar);
                        System.out.println("Estudiante actualizado correctamente");
                    } else {
                        System.out.println("Estudiante no encontrado");
                    }
                    break;
                // Lógica para eliminar estudiante
                case 3:
                	System.out.print("Ingrese el número de identificación del estudiante a eliminar: ");
                    String idEstudianteEliminar = scanner.next();
                    
                    estudianteDAO.eliminarEstudiante(idEstudianteEliminar);
                    System.out.println("Estudiante eliminado correctamente");
                    break;
                // Lógica para agregar nota
                case 4:
                	Scanner notaScanner = new Scanner(System.in);

                    System.out.println("Ingrese los datos de la nota:");
                    System.out.print("ID del estudiante: ");
                    int idEstudiante = notaScanner.nextInt();
                    notaScanner.nextLine(); // Limpiar el buffer
                    System.out.print("Asignatura: ");
                    String asignatura = notaScanner.nextLine();
                    System.out.print("Grado: ");
                    String grado1 = notaScanner.nextLine();
                    System.out.print("Nota definitiva: ");
                    double notaDefinitiva = notaScanner.nextDouble();
                    notaScanner.nextLine(); // Limpiar el buffer
                    
                    Nota nuevaNota = new Nota(idEstudiante, asignatura, grado1, notaDefinitiva);
                    notaDAO.insertarNota(nuevaNota);
                    System.out.println("Nota agregada correctamente");
                    break;
                // Lógica para actualizar nota
                case 5:
                	Scanner updateNotaScanner = new Scanner(System.in);

                    System.out.print("Ingrese el ID del estudiante de la nota a actualizar: ");
                    int idEstudianteActualizar1 = updateNotaScanner.nextInt();
                    updateNotaScanner.nextLine(); // Limpiar el buffer
                    System.out.print("Ingrese la asignatura de la nota a actualizar: ");
                    String asignaturaActualizar = updateNotaScanner.nextLine();

                    Nota notaActualizar = notaDAO.consultarNota(idEstudianteActualizar1, asignaturaActualizar);

                    if (notaActualizar != null) {
                        System.out.println("Ingrese los nuevos datos de la nota:");

                        System.out.print("Nuevo grado: ");
                        String nuevoGrado = updateNotaScanner.nextLine();
                        System.out.print("Nueva nota definitiva: ");
                        double nuevaNotaDefinitiva = updateNotaScanner.nextDouble();
                        updateNotaScanner.nextLine(); // Limpiar el buffer
                        
                        // Actualizar los datos de la nota consultada
                        notaActualizar.setGrado(nuevoGrado);
                        notaActualizar.setNotaDefinitiva(nuevaNotaDefinitiva);

                        notaDAO.actualizarNota(notaActualizar);
                        System.out.println("Nota actualizada correctamente");
                    } else {
                        System.out.println("Nota no encontrada");
                    }
                    break;
                // Lógica para eliminar nota     
                case 6:
                	System.out.print("Ingrese el ID del estudiante de la nota a eliminar: ");
                    int idEstudianteNotaEliminar = scanner.nextInt();
                    System.out.print("Ingrese la asignatura de la nota a eliminar: ");
                    String asignaturaNotaEliminar = scanner.next();
                    
                    notaDAO.eliminarNota(idEstudianteNotaEliminar, asignaturaNotaEliminar);
                    System.out.println("Nota eliminada correctamente");
                    break;
                case 7:
                    continuar = false;
                    break;
                default:
                    System.out.println("Opción no válida");
            }
        }

        scanner.close();
    }
}
