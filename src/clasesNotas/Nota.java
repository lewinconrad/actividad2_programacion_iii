package clasesNotas;

public class Nota {
    private int idEstudiante;
    private String asignatura;
    private String grado;
    private double notaDefinitiva;

    public Nota(    int idEstudiante, 
                    String asignatura, 
                    String grado, 
                    double notaDefinitiva) 
    {
        this.idEstudiante = idEstudiante;
        this.asignatura = asignatura;
        this.grado = grado;
        this.notaDefinitiva = notaDefinitiva;
    }

    // Getters y Setters
    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public double getNotaDefinitiva() {
        return notaDefinitiva;
    }

    public void setNotaDefinitiva(double notaDefinitiva) {
        this.notaDefinitiva = notaDefinitiva;
    }
}
